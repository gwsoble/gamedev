﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VelocityScript : MonoBehaviour {

	public float startSped = 50f;
	// Use this for initialization
	void Start () {
		Rigidbody rigidBody = GetComponent<Rigidbody>();
		rigidBody.velocity = new Vector3(startSped, 0, startSped);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
