﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class GameManager : MonoBehaviour
{
	public Text scoreText;
	public Text gameOverText;
	public Animator gameOverAnimator;
	public GameObject _obstacle;
	public GameObject _player;
	public Button _button;
	public GameObject obsManager;

	int score;
	float timePassed;
	

	// Start is called before the first frame update
	private void Start()
	{
		score = 0;
		timePassed = 0;
		_button.onClick.AddListener(ClickPlayAgain);
		_obstacle = GameObject.FindGameObjectWithTag("obstacle");
		_player = GameObject.FindGameObjectWithTag("Player");

	}
	public void GameOver()
	{
		gameOverText.text = "SCORE: " + score;
		gameOverAnimator.SetBool("isGameOver", true);
		GameObject.Destroy(_player);
		GameObject.Destroy(_obstacle);

	}

	public void ClickPlayAgain()
	{
		SceneManager.LoadScene("Main");
	}

	private void Update()
	{
		timePassed += Time.deltaTime;
		if (timePassed >= 1.0f)
		{
			timePassed = 0f;
			score += 1;
		}
		scoreText.text = "SCORE: " + score.ToString();
	}
}
