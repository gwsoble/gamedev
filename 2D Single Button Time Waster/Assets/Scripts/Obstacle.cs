﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour
{
	Vector3 initialPosition;
    // Start is called before the first frame update
    void Start()
    {
		initialPosition = transform.position;
	}

    // Update is called once per frame
    void Update()
    {
		transform.Translate(Vector2.left * 4 * Time.deltaTime);
		if (transform.position.y < -5)
			Destroy(gameObject);
    }
}
