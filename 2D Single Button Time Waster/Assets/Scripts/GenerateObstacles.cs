﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerateObstacles : MonoBehaviour
{
	public GameObject obstacle;
	float timer;
	public float spawnTime;
	bool topTurn;

    // Start is called before the first frame update
    void Start()
    {
		topTurn = false;
		timer = 0;
    }

    // Update is called once per frame
    void Update()
    {
		manageTimer();
    }

	void manageTimer()
	{
		timer += Time.deltaTime;
		if (timer >= spawnTime)
		{
			addObstacle();
			timer = 0;
			spawnTime -= 0.05f;
		}
		if (spawnTime < .5f)
			spawnTime = .5f;
	}

	void addObstacle()
	{
		float r = Random.Range(1, 5);
		Vector3 pos = GameObject.Find("Player").GetComponent<ControlPlayer>().initialPosition;
		Vector3 negPos = GameObject.Find("Player").GetComponent<ControlPlayer>().initialPosition;
		negPos.y *= -1;
		GameObject temp;
		if (topTurn)
		{
			temp = GameObject.Instantiate(obstacle, negPos + Vector3.right * 20, Quaternion.identity);
			temp.transform.localScale = new Vector3(1, r, 1);
			topTurn = false;
		}
		else
		{
			temp = GameObject.Instantiate(obstacle, pos + Vector3.right * 20, Quaternion.identity);
			temp.transform.localScale = new Vector3(1, r, 1);
			topTurn = true;
		}
	}
}
