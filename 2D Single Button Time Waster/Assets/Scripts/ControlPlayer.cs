﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlPlayer : MonoBehaviour
{
	bool isOnGround;
	public Vector3 initialPosition;
	public Animator gameOverAnimation;

	// Start is called before the first frame update
	private void OnCollisionEnter2D(Collision2D collision)
	{
		if (collision.collider.tag == "ground")
			isOnGround = true;
		else if (collision.collider.tag == "obstacle")
		{
			isOnGround = false;
			Object.FindObjectOfType<GameManager>().GameOver();
		}
		else
			isOnGround = false;
	}
	private void OnCollisionExit2D(Collision2D collision)
	{
		isOnGround = false;
	}
	void SwitchGravity()
	{
		GetComponent<Rigidbody2D>().gravityScale *= -1;
	}

	private void Update()
	{
		if (Input.GetKeyDown(KeyCode.Space) && isOnGround)
			SwitchGravity();

	}

	private void Start()
	{
		initialPosition = transform.position;
	}
}
