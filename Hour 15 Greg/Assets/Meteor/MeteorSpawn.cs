﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeteorSpawn : MonoBehaviour {

	public GameObject meteorPrefab;
	public float minSpawnDelay = 1f;
	public float maxSpanwDelay = 3f;
	public float spawnXLimit = 6f;

	void Spawn()
	{
		float random = Random.Range(-spawnXLimit, spawnXLimit);
		Vector3 spawnPos = transform.position + new Vector3(random, 0f, 0f);
		Instantiate(meteorPrefab, spawnPos, Quaternion.identity);

		Invoke("Spawn", Random.Range(minSpawnDelay, maxSpanwDelay));
	}

	// Use this for initialization
	void Start () {
		Spawn();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
