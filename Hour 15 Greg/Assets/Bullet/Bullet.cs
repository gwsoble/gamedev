﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

	public float speed = 10f;

	GameManager gameManager;  //private this time around

	// Use this for initialization
	void Start () {
		gameManager = GameObject.FindObjectOfType<GameManager>();

		Rigidbody2D rigidbody = GetComponent<Rigidbody2D>();
		rigidbody.velocity = new Vector2(0f, speed);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	private void OnCollisionEnter2D(Collision2D collision)
	{
		Destroy(collision.gameObject);
		gameManager.AddScore();
		Destroy(gameObject);
	}
}
