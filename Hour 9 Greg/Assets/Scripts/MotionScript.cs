﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MotionScript : MonoBehaviour {

	const float speedMult = 0.05f;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		float hVal = Input.GetAxis("Horizontal");
		float vVal = Input.GetAxis("Vertical");

		if (hVal != 0)
		{
			float hSpeed = speedMult * hVal;
			transform.Translate(hSpeed, 0, 0);
		}
		if (vVal != 0)
		{
			float vSpeed = speedMult * vVal;
			transform.Translate(0, vSpeed, 0);
		}
	}
}
