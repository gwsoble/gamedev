﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerScript : MonoBehaviour {


	private void OnTriggerEnter(Collider other)
	{
		print("Entering trigger of " + gameObject.name);
	}
	private void OnTriggerStay(Collider other)
	{
		print("Staring in trigger of " + gameObject.name);
	}
	private void OnTriggerExit(Collider other)
	{
		print("Exiting trigger of " + gameObject.name);
	}
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
