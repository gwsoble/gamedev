﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInput : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
		float hVal = Input.GetAxis("Horizontal");
		float vVal = Input.GetAxis("Vertical");

		if (hVal != 0)
			print("Horizontal movement selected :" + hVal);

		if (vVal != 0)
			print("Vertical movement selected :" + vVal);

		if (Input.GetKey(KeyCode.M))
			print("M key select4d");

		if (Input.GetKey(KeyCode.O))
			print("O key select4d");

		float mxVal = Input.GetAxis("Mouse X");
		float myVal = Input.GetAxis("Mouse Y");
		if (mxVal != 0)
			print("Mouse X movement Selected: " + mxVal);
		if (myVal != 0)
			print("Mouse Y movement Selected: " + myVal);
	}
}
